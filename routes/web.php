<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('adminlte.master');
});

Route::get('/table', function () {
    return view('items.index');
});

Route::get('/data-tables', function () {
    return view('items.create');
});

Route::get('/cast/create', 'CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast','CastController@index');
Route::get('/cast/{id}','CastController@show');
Route::get('cast/{id}/update', 'CastController@update');
Route::put('/cast/{id}', 'CastController@ubah');
Route::delete('/cast/{id}', 'CastController@destroy');