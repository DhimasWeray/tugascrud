<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }
    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio'  => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('cast')->with('success','Data Berhasil Disimpan!');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        // dd($cast);

        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $post = DB::table('cast')->where('id',$id)->first();
        return view('cast.show',compact('post'));

    }

    public function update($id){
        $post = DB::table('cast')->where('id',$id)->first();

        return view('cast.update', compact('post'));
    }

    public function ubah($id, Request $request){
         $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio'  => 'required']);

        $query = DB:: table('cast')
        ->where('id',$id)
        ->update([
            'nama'=> $request['nama'],
            'umur'=> $request['umur'],
            'bio'=> $request['bio']
        ]);

        return redirect('/cast')->with('success','Berhasil Update Data!');
    }

    public function destroy($id){
        $query = DB::table('cast')->where('id',$id)->delete();

        return redirect('/cast')->with('success','Berhasil Dihapus!');
    }


}