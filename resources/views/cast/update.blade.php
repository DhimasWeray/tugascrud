@extends('adminlte.master')

@section('content')
    <form role="form" action="/cast/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama',$post->nama)}}" >
                    @error('nama')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" value="{{old('umur',$post->umur)}}" >
                    @error('umur')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror                    
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">BIO</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{old('bio',$post->bio)}}" >
                    @error('bio')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
@endsection