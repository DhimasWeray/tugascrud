@extends('adminlte.master')

@section('content')
    <form role="form" action="/cast" method="POST">
        @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama','')}}" placeholder="nama">
                    @error('nama')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" value="{{old('umur','')}}" placeholder="12">
                    @error('umur')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror                    
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">BIO</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{old('bio','')}}" placeholder="bio">
                    @error('bio')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
@endsection