@extends('adminlte.master')

@section('content')

    <div class="mt-3 ml-3">
        <div class="card">
            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
                
            @endif
              <div class="card-header">
                <h3 class="card-title">Tabel CAST</h3>
              </div>
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>BIO</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($cast as $key=> $post)
                        <tr>
                            <td> {{$key + 1}}</td>
                            <td> {{$post->nama}}</td>
                            <td> {{$post->umur}}</td>
                            <td> {{$post->bio}}</td>
                            <td style="display: flex">
                                <a class="btn btn-info btn-sm mr-2" href="/cast/{{$post->id}}">Show</a>
                                <a class="btn btn-default btn-sm mr-2" href="/cast/{{$post->id}}/update">Update</a>
                                <form action="/cast/{{$post->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                           <td> Data Kosong</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
                <a href="/cast/create" class="btn btn-primary mt-3 ml-3 mb-2">Add Data</a>
              </div>
            </div>

    </div>
    
@endsection